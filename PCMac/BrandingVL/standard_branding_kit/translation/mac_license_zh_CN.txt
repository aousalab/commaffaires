<center>IMPORTANT - READ CAREFULLY</center><br>
<br>
THIS IS A LEGAL AGREEMENT BETWEEN YOU (THE INDIVIDUAL OR THE ENTITY) AND THE PROVIDER OF THE SOFTWARE (<b>"LICENSOR"</b>) REGARDING YOUR USE OF THE SOFTWARE. PLEASE READ THE FOLLOWING TERMS CAREFULLY.<br>
<br>
<br>
Use of the software and documentation (the <b>"Product"</b>) is contingent on acceptance and agreement by You to the terms and conditions set out below. You may not use the Product in any way unless you have accepted these terms and conditions.<br>
<br>
BY CLICKING ON THE "ACCEPT" BUTTON, YOU ARE CONSENTING TO BE BOUND BY THIS AGREEMENT. IF YOU DO NOT AGREE TO ALL OF THE TERMS OF THIS END-USER LICENSE AGREEMENT (<b>"EULA"</b>), CLICK THE <b>"DECLINE"</b> BUTTON.<br>
<br>
The Product is not a replacement for Your mobile or fixed line telephone. In particular, the Product does not allow you to make emergency calls to emergency services. You must make alternative communications arrangements to ensure that You can make emergency calls if needed.<br>
<br>
<b>1.	GRANT OF LICENSE.</b><br>
<br>
Subject to the conditions and limitations below, Licensor grants to You a personal, non-exclusive, non-transferable, non-sublicensable, limited license in object code form only, to use one copy of the executable code of the Product on a single Device used by You. (A Device is a personal computer or mobile device.) You agree not to copy the Product, including its software and documentation. You also acknowledge that the Product contains valuable trade secrets proprietary information belonging to Licensor and others. Accordingly, you shall take measures to protect the Product from unauthorized access, disclosure and use, including without limitation the placement of intellectual property or any other proprietary rights notices on the Product and other materials supplied by the Licensor as stated in Article 2 below. All other rights are reserved to Licensor. You shall not rent, lease, sell, sublicense, assign, or otherwise transfer the Product, including any accompanying printed materials.<br>
<br>
<b>2.	 INTELLECTUAL PROPERTY RIGHTS.</b><br>
<br>
The Product is a proprietary product of Licensor and several suppliers to Licensor and is protected by various intellectual property laws, including copyright law.  You acquire only the right to use the Product and may not use the software and documentation otherwise than as a part of the Product in which the software and documentation have been incorporated or as they have been delivered. You shall not disclose the results of any benchmark tests of the Product to any third party without Licensor's prior written approval.<br>
<br>
Further, You agree not to, or to allow others to (i) adapt, alter, modify, decompile, translate, make derivative works, disassemble,
or reverse engineer the Product, including without limitation the source code and any other underlying ideas or algorithms (except to
the extent applicable laws specifically prohibit such restriction); (ii) create license keys that enable the Product; (iii) copy the
Product; (iv) use the Product in connection with dangerous activities such as the operation of nuclear facilities, air traffic control
or life support, where the failure of the Product could lead to death, personal injury, or extensive environmental damage;
(v) sublicense, transfer or otherwise grant any rights in the Product, loan, sell, lease, rent, or otherwise commercially use or exploit
the Product, in whole or in part; (vi) use the Product in violation of any applicable regulation or law; or (vii) export or transfer
the Product or any component thereof in violation of any export control laws.
For the avoidance of doubt, nothing in this EULA grants to You any rights to the source code of the Product.<br>
<br>3.	CONSENT TO USE OF DATA. </b><br>
<br>
You agree that Licensor may collect and use technical data and related information, including but not limited to technical information about Your device, system and application software, and peripherals, that is gathered periodically to facilitate the provision of software updates, product support and other services to You (if any) related to the Product. Licensor may use this information, including sharing it with third parties, as long as it is in a form that does not personally identify You, to improve its products or to provide services or technologies to You. <br>
<br>4.	 PRODUCT MAINTENANCE.</b><br>
<br>
Licensor is not obligated to provide maintenance, support or updates to You for the Product.<br>
<br>
<b>5.	 DISCLAIMER OF WARRANTY.</b><br>
<br>
PRODUCT IS DEEMED ACCEPTED BY YOU UPON ACCEPTANCE OF THIS EULA. THE PRODUCT IS PROVIDED "AS IS" WITHOUT WARRANTY, EXPRESS OR IMPLIED, OF ANY KIND. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, LICENSOR AND ITS SUPPLIERS FURTHER DISCLAIM ALL WARRANTIES, INCLUDING WITHOUT LIMITATION ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NONINFRINGEMENT, INTERFERENCE, INFORMATIONAL CONTENT AND SYSTEM INTEGRATION. THE ENTIRE RISK ARISING OUT OF THE USE OR PERFORMANCE OF THE PRODUCT AND DOCUMENTATION REMAINS WITH YOU. THIS DISCLAIMER OF WARRANTY IS AN ESSENTIAL PART OF THIS EULA. LICENSOR AND ITS SUPPLIERS DO NOT WARRANT THAT THE PRODUCT WILL MEET YOUR REQUIREMENTS OR THAT THE OPERATION OF THE PRODUCT WILL BE ERROR FREE. BECAUSE SOME STATES/JURISDICTIONS DO NOT ALLOW THE EXCLUSIONS OF AN IMPLIED WARRANTY, THIS DISCLAIMER MAY NOT APPLY TO YOU AND YOU MAY HAVE OTHER LEGAL RIGHTS THAT VARY FROM STATE TO STATE OR BY JURISDICTION.<br>
<br>
<b>6.	 LIMITATION OF LIABILITY, INDEMNIFICATION</b><br>
<br>
THE LIABILITY OF LICENSOR OR OF ITS SUPPLIERS ARISING OUT OF THIS EULA SHALL NOT EXCEED THE AMOUNTS PAID BY RECIPIENT TO OBTAIN
THE PRODUCT. UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL THEORY, TORT, CONTRACT, OR OTHERWISE, SHALL LICENSOR, OR ITS SUPPLIERS OR
AGENTS BE LIABLE TO YOU OR ANY OTHER PERSON FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES OF ANY CHARACTER
INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF GOODWILL, WORK STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, OR ANY AND ALL OTHER
COMMERCIAL DAMAGES OR LOSSES, EVEN IF LICENSOR SHALL HAVE BEEN INFORMED OF THE POSSIBILITY OF SUCH DAMAGES, OR FOR ANY CLAIM BY ANY
OTHER PARTY OR WHETHER OR NOT THE POSSIBILITY OF SUCH DAMAGES COULD HAVE BEEN REASONABLE FORESEEN. THIS LIMITATION OF LIABILITY SHALL
NOT APPLY TO LIABILITY FOR DEATH OR PERSONAL INJURY TO THE EXTENT APPLICABLE LAW PROHIBITS SUCH LIMITATION. FURTHERMORE, SOME
JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES,
SO THIS LIMITATION AND EXCLUSION MAY NOT APPLY TO YOU. THESE LIMITATIONS WILL APPLY EVEN IF LICENSOR OR ITS AGENT HAS BEEN ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE. YOU ACKNOWLEDGE THAT THE AMOUNT PAID FOR THE PRODUCT REFLECTS THIS ALLOCATION OF RISK. RECIPIENT
AGREES TO HOLD HARMLESS, INDEMNIFY AND DEFEND LICENSOR AND ITS OFFICERS, DIRECTORS, EMPLOYEES AND SUPPLIERS, FROM ANY AND AGAINST ANY
LOSSES, DAMAGES, FINES AND EXPENSES (INCLUDING ATTORNEYS' FEES AND COSTS) ARISING OUT OF OR RELATING TO USE OF THE PRODUCT IN VIOLATION
OF ANOTHER PARTY'S RIGHT OR IN VIOLATION OF ANY LAW.<br>
<br>
<b>7.	 SPECIFIC DISCLAIMER OF LIABILITY FOR EMERGENCY SERVICES</b><br>
<br>
NEITHER LICENSOR NOR ITS OFFICERS, EMPLOYEES OR AFFILIATES MAY BE HELD LIABLE WHETHER IN CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE), OR ANY OTHER FORM OF LIABILITY FOR ANY CLAIM, DAMAGE, OR LOSS, (AND YOU HEREBY WAIVE ANY AND ALL SUCH CLAIMS OR CAUSES OF ACTION), ARISING FROM OR RELATING TO:<br>
<br>
(I) YOUR INABILITY TO USE THE PRODUCT TO CONTACT EMERGENCY SERVICES, OR<br>
<br>
(II) YOUR FAILURE TO MAKE ADDITIONAL ARRANGEMENTS TO ACCESS EMERGENCY SERVICES;<br>
<br>
<b>8.	EXPORT.</b><br>
<br>
You agree to comply fully with all laws and regulations to assure that the Product is NOT exported, directly or indirectly, in violation of any applicable law and/or jurisdiction, including the United States.  You also agree that You will not export or re-export the Product in any form without the appropriate government licenses, including the United States and understand that the Product may not be exported or re-exported (a) into any U.S embargoed country, or (b) to anyone on the U.S. Treasury Department’s list of Specially Designated Nationals or the U.S. Department of Commerce Denied Person’s List or Entity List. By using the Product You represent and warrant that You are not located in any such country or on any such list.  Your failure to comply with this provision is a material breach of this EULA.<br>
<br>
<b>9. 	U.S. GOVERNMENT USE.</b><br>
<br>
The Product and accompanying documentation are deemed to be "commercial computer software" and "commercial computer software documentation," respectively, pursuant to DFAR Section 227.7202 and FAR Section 12.212, as applicable. Any use, modification, reproduction, release, performing, displaying or disclosing of the Product and accompanying documentation by the U. S. Government shall be governed solely by these terms of the License and shall be prohibited except to the extent expressly permitted by these terms of the License.<br>
<br>
<b>10.	ENTIRE AGREEMENT.</b><br>
<br>
This EULA constitutes the complete and exclusive agreement between Licensor and Recipient with respect to the subject matter hereof, and supersedes all prior oral or written understandings, communications or agreements not specifically incorporated herein, if any. This EULA may not be modified except in writing duly signed by an authorized representative of Licensor. If any provision of this EULA is held to be unenforceable for any reason, such provision shall be altered only to the extent necessary to make it enforceable, and such decision shall not affect the enforceability of such provision under other circumstances, or of the remaining provisions hereof under all circumstances.<br>
<br>
<b>11. 	TERMINATION.</b><br>
<br>
This EULA is effective until terminated. You may terminate this EULA at any time by removing from your Device the Product and destroying all copies of the Product and the accompanying documentation in your possession. Unauthorized copying of the Product or the accompanying documentation or otherwise failing to comply with the terms and conditions of this EULA will result in automatic termination of this EULA and will make available to Licensor other legal remedies. Upon termination of this EULA, the license granted herein will terminate and you must immediately destroy the Product and accompanying documentation, and all back-up copies thereof.<br>
<br>
<b>12.      GENERAL LEGAL TERMS.</b><br>
<br>
This EULA shall be governed by the substantive laws of the State of Maryland, USA, without regard to its conflicts of laws principles. You hereby submit to the exclusive jurisdiction of the federal and state courts in the State of Maryland in connection with any dispute arising out of this EULA. The United Nations Convention on Contracts for the International Sale of Goods shall not apply to this EULA and is hereby expressly excluded. The failure of Licensor to exercise or enforce any right or provision of this EULA shall not constitute a waiver of such right or provision. Sections 2, 3, 4, 6, 7, 8, 9, 10, and 11  shall survive the termination of this EULA.<br>
<br>
<br> Licensor’s Privacy Policy can be found at http://www.broadsoft.com/privacy/.<br>
<br>
<br>
<b>13.      THIRD PARTIES.</b><br>
<br>
SQLCipher<br>
Copyright (c) 2008-2012 Zetetic LLC<br>
All rights reserved.<br>
<br>
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:<br>
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.<br>
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.<br>
* Neither the name of the ZETETIC LLC nor the
names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.<br>
<br>
THIS SOFTWARE IS PROVIDED BY ZETETIC LLC ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL ZETETIC LLC BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.<br>
<br>
<br>
WebRTC<br>
Copyright (c) 2011, The WebRTC project authors. All rights reserved.<br>
<br>
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:<br>
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.<br>
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in
the documentation and/or other materials provided with the
distribution.<br>
* Neither the name of Google nor the names of its contributors may
be used to endorse or promote products derived from this software
without specific prior written permission.<br>
<br>
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.<br>
<br>
<br>
OpenH264<br>
Copyright (c) 2013, Cisco Systems<br>
All rights reserved.<br>
<br>
OpenH264 License: https://github.com/cisco/openh264/blob/master/LICENSE
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:<br>
* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.<br>
* Redistributions in binary form must reproduce the above copyright notice, this
list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.<br>
<br>
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.<br>
<br>
<br>
Blowfish - Based on a C implementation of the Blowfish algorithm by Paul Kocher<br>
<br>
<br>
UUID - RFC4412<br>
/*<br>
** Copyright (c) 1990- 1993, 1996 Open Software Foundation, Inc.<br>
** Copyright (c) 1989 by Hewlett-Packard Company, Palo Alto, Ca. &<br>
** Digital Equipment Corporation, Maynard, Mass.<br>
** Copyright (c) 1998 Microsoft.<br>
** To anyone who acknowledges that this file is provided "AS IS"<br>
** without any express or implied warranty: permission to use, copy,<br>
** modify, and distribute this file for any purpose is hereby<br>
** granted without fee, provided that the above copyright notices and<br>
** this notice appears in all source code copies, and that none of<br>
** the names of Open Software Foundation, Inc., Hewlett-Packard<br>
** Company, Microsoft, or Digital Equipment Corporation be used in<br>
** advertising or publicity pertaining to distribution of the software<br>
** without specific, written prior permission. Neither Open Software<br>
** Foundation, Inc., Hewlett-Packard Company, Microsoft, nor Digital<br>
** Equipment Corporation makes any representations about the<br>
** suitability of this software for any purpose.<br>
*/<br>
<br>
<br>
MD5<br>
Copyright (c) 1991-2, RSA Data Security, Inc. Created 1991. All rights reserved.<br>
<br>
License to copy and use this software is granted provided that it is identified as the "RSA Data Security, Inc. MD5 Message-Digest Algorithm" in all material mentioning or referencing this software or this function.<br>
<br>
License is also granted to make and use derivative works provided that such works are identified as "derived from the RSA Data Security, Inc. MD5 Message-Digest Algorithm" in all material mentioning or referencing the derived work.<br>
<br>
RSA Data Security, Inc. makes no representations concerning either the merchantability of this software or the suitability of this software for any particular purpose. It is provided "as is" without express or implied warranty of any kind.<br>
<br>
<br>
SHA-1<br>
Copyright (c) The Internet Society (2001).  All Rights Reserved.<br>
<br>
This document and translations of it may be copied and furnished to
others, and derivative works that comment on or otherwise explain it
or assist in its implementation may be prepared, copied, published
and distributed, in whole or in part, without restriction of any
kind, provided that the above copyright notice and this paragraph are
included on all such copies and derivative works.  However, this
document itself may not be modified in any way, such as by removing
the copyright notice or references to the Internet Society or other
Internet organizations, except as needed for the purpose of
developing Internet standards in which case the procedures for
copyrights defined in the Internet Standards process must be
followed, or as required to translate it into languages other than
English.<br>
<br>
The limited permissions granted above are perpetual and will not be
revoked by the Internet Society or its successors or assigns.<br>
<br>
This document and the information contained herein is provided on an
"AS IS" basis and THE INTERNET SOCIETY AND THE INTERNET ENGINEERING
TASK FORCE DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO ANY WARRANTY THAT THE USE OF THE INFORMATION
HEREIN WILL NOT INFRINGE ANY RIGHTS OR ANY IMPLIED WARRANTIES OF
MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.<br>
<br>
<br>
This application contains some software licensed under the "GNU LESSER GENERAL PUBLIC LICENSE, Version 3" 
provided with ABSOLUTELY NO WARRANTY under the terms of "GNU LESSER GENERAL PUBLIC LICENSE, Version 3", 
available here: http://www.gnu.org/licenses/lgpl.html.
<br>
<br>
<br>
The following 3rd party libraries are in use based on LGPL license.<br>
<br>
This software uses Qt libraries licensed under the GNU Lesser General Public License version 3.<br>
<br>
This software uses parts of Qt Toolkit.<br>
The Qt Toolkit is Copyright (c) 2015 The Qt Company Ltd and other contributors.<br>
Contact: http://www.qt.io/licensing/<br>
<br>
<br>
Qjson - Copyright 2012 - Flavio Castelli<br>
<br>
<br>
Qxmpp - Copyright 2008-2011 The QXmpp developers<br>
<br>
<br>
QCA - Copyright 2003-2007 Justin Karneges &lt;justin@affinix.com&gt;, Copyright 2004-2006 Brad Hards &lt;bradh@frogmouth.net&gt;<br>
<br>
<br>
LGPL license are available at:<br>
http://www.gnu.org/copyleft/lesser.html<br>
LGPL License is also available locally with this software, at the installation directory.<br>
<br>
<br>
<br>
The following software is used based on BSD license:<br>
<br>
OpenSSL - Copyright 1998-2011 The OpenSSL Project<br>
License available at:<br>
http://www.openssl.org/source/license.html<br>
<br>
<br>
Breakpad - Copyright 1998, Regents of the University of California<br>
Breakpad license available at:<br>
http://opensource.org/licenses/BSD-3-Clause<br>
<br>
<br>
Stackwalker - Copyright 2009, Jochen Kalmbach<br>
License available at:<br>
http://stackwalker.codeplex.com/license<br>
<br>
<br>
<br>
The following 3rd party libraries are using Apache license 2.0:<br>
<br>
Log4cxx - Copyright 2012 Apache Software Foundation.<br>
License available at:<br>
http://logging.apache.org/log4cxx/license.html<br>
<br>
<br>
<br>
The following 3rd party library is used based on its own non-restrictive license:<br>
<br>
OpenLDAP - Copyright 2013, OpenLDAP Foundation.<br>
OpenLDAP license is available online for review:<br>
http://www.openldap.org/software/release/license.html<br>
OpenLDAP license is also available locally with this software at the installation directory.<br>
<br>
<br>
<br>
The following free software used only when 3rd party software is using BroadTouch Business Communicator API, via LGPL, MIT, BSD or other license:<br>
<br>
Tufao (LGPL2.1) - Copyright 2012 Vinicius dos Santos Oliveira &lt;vini.ipsmaker@gmail.com&gt;<br>
<br>
<br>
Libcurl - Copyright 1996 - 2013, Daniel Stenberg, &lt;daniel@haxx.se&gt;<br>
License available at:<br>
http://curl.haxx.se/docs/copyright.html<br>
<br>
<br>
Tinythread++ - Copyright 2012 Marcus Geelnard<br>
License available at:<br>
http://opensource.org/licenses/zlib-license.php<br>
<br>
<br>
Picojson - Copyright  2009-2010 Cybozu Labs, Inc. Copyright (c) 2011 Kazuho Oku<br>
License available at:<br>
https://github.com/kazuho/picojson/blob/master/LICENSE<br>
<br>
<br>
Easywsclient - Copyright 2012, 2013 &lt;dhbaird@gmail.com&gt;<br>
License available at:<br>
https://github.com/dhbaird/easywsclient/blob/master/COPYING<br>
<br>
<br>
<br>
<br>Websocket-sharp - Copyright 2012, 2013
<br>
<br>
<br>
<br>Emoji artwork provided by EmojiOne. 
<br>http://emojione.com
<br>
<br>
<br>
<br>
Copyright 2005-2018 BroadSoft, Inc. and/or its Original licensors.  All rights reserved.