README
-----------

1) Starting from v.10.0.4, please use the following line in the btbc_build.ini to differentiate customers:
customer_code=XYZ

2) 10.1.0 branding kit introduces /theme/sounds-folder, which contains brandable audio tones. These are optional branding items.

3) 20.0.0 requires two additional rows in btbc_build.ini to control presence of headphone usb connectors and outlook plugin. Add following rows to your old btbc_build.ini or use the provided example as basis:
add_headphones_connectors=false
add_outlook_plugin=false

4) R20 branding kit contains extra images that are optionally brandable, minimal set familiar from r10 is still enough in most cases.

5) 20.1.0 introduces new optional variable; 
forgot_password_url=

6) 22.0.0 introduces support for three levels of branding: simple, standard and advanced. This kit contains all three variants as examples and additionally the kit used for producing Skype for Business add-in variant. The Advanced example and Skype for Business example also contain all the image resources that can be modified. Note: It is possible to combine basic color definition but replace all icons, just mix and match with the provided kits.

7) 22.4.0 example kits no longer contain the emoji/emoticon images due changed implementation to support emojis, see Branding Guide for extended explanation.

8) Starting in 22.5.0 release, branding parameter sso_context is added. To maintain existing login window behaviour, this should be set to have value "broadsoft". The examples are set like this. See Branding Guide for additional details how this parameter is used with newly introduced SSO-login options.


