;--------------------------------
;Installer initial page texts

LangString INSTALLERLANGUAGETITLE ${LANG_LATINAMERICANSPANISH} "Idioma del instalador"
LangString INSTALLERLANGSELECTIONTITLE ${LANG_LATINAMERICANSPANISH} "Seleccione un idioma."


;--------------------------------
;Uninstall old version question

LangString OLDVERSIONWARNING ${LANG_LATINAMERICANSPANISH} "Se encontró en su sistema una versión existente de $(^Name). Se recomienda que desinstale la versión existente antes de instalar la nueva versión.$\r$\n$\r$\n¿Desea desinstalar la versión existente de $(^Name)?"


;--------------------------------
;Uninstall old version failed

LangString OLDVERSIONREMOVEERROR ${LANG_LATINAMERICANSPANISH} "Se encontró un problema al eliminar la versión existente de $(^Name). Desinstálela manualmente con Programas y características del Panel de control."


;--------------------------------
;Install options page title

LangString INSTALLOPTIONSTITLE ${LANG_LATINAMERICANSPANISH} "Opciones de instalación"


;--------------------------------
;Install options page header text

LangString INSTALLOPTIONSHEADER ${LANG_LATINAMERICANSPANISH} "Seleccione las opciones para la instalación de $(^Name)"


;--------------------------------
;Install options page, option run when system starts

LangString RUNWHENSYSTEMSTARTS ${LANG_LATINAMERICANSPANISH} "Ejecutar siempre $(^Name) al iniciar el sistema"


;--------------------------------
;Install options page, option ceate desktop icon

LangString CREATEDESKTOPICON ${LANG_LATINAMERICANSPANISH} "Crear icono de $(^Name) en el escritorio"

;Install options page, option to install for all users

LangString INSTALLFORALLUSERS ${LANG_LATINAMERICANSPANISH} "Instalar para todos los usuarios"

;--------------------------------
;Install finish page, options start application now

LangString STARTAPPLICATIONNOW ${LANG_LATINAMERICANSPANISH} "Iniciar $(^Name) ahora"


;--------------------------------
;Error message when uninstalling and application still running

LangString APPLICATIONSTILLRUNNING ${LANG_LATINAMERICANSPANISH} "$(^Name) se sigue ejecutando. Cierre la aplicación y ejecute el desinstalador de nuevo."

;--------------------------------
;Question when client will crash with the call window

LangString BADDISPLAYDRIVERCONTINUEQUSTION ${LANG_LATINAMERICANSPANISH} "Para poder utilizar todas las características de $(^Name), debe actualizar los controladores del adaptador de pantalla. Puede usar $(^Name) sin actualizarlo, pero es posible que no pueda generar vídeo acelerado por hardware.$\r$\n$\r$\n¿Desea continuar con la instalación?"


;--------------------------------
;Error message when the call window crashes and user decides to abort installation

LangString INSTALLATIONABORTEDBADDISPLAYDRIVER ${LANG_LATINAMERICANSPANISH} "Instalación de $(^Name) anulada"


;--------------------------------
; Normal installation component name

LangString NORMALINSTALLATIONCOMPONENTNAME ${LANG_LATINAMERICANSPANISH} "Instalación normal"


;--------------------------------
; Portable installation component name

LangString PORTABLEINSTALLATIONCOMPONENTNAME ${LANG_LATINAMERICANSPANISH} "Instalación portátil"


;--------------------------------
; Normal installation component description

LangString NORMALSECTIONDESC ${LANG_LATINAMERICANSPANISH} "Instalar y registrar el programa en el sistema operativo"


;--------------------------------
; Portable installation component description

LangString PORTABLESECTIONDESC ${LANG_LATINAMERICANSPANISH} "Desempaquetar el programa en el directorio especificado sin hacer ningún cambio en el sistema operativo"


;--------------------------------
; Normal installation component name and description

LangString NORMALINSTALLATIONCOMPONENTNAMEANDDESC ${LANG_LATINAMERICANSPANISH} "Instalación normal. Instalar y registrar el programa en el sistema operativo."


;--------------------------------
; Portable installation component name and description

LangString PORTABLEINSTALLATIONCOMPONENTNAMEANDDESC ${LANG_LATINAMERICANSPANISH} "Instalación portátil. Desempaquetar el programa en el directorio especificado sin hacer ningún cambio en el sistema operativo."


;--------------------------------
; Install mode page title

LangString INSTALLMODETITLE ${LANG_LATINAMERICANSPANISH} "Modo de instalación"


;--------------------------------
; Install mode page header text

LangString INSTALLMODEHEADER ${LANG_LATINAMERICANSPANISH} "Seleccione el modo de instalación de $(^Name)"


;--------------------------------
; Installation details info: environment for all users

LangString INFOSELECTEDALLENV ${LANG_LATINAMERICANSPANISH} "Entorno seleccionado para todos los usuarios"


;--------------------------------
; Installation details info: environment for current user


LangString INFOSELECTEDUSERENV ${LANG_LATINAMERICANSPANISH} "Entorno seleccionado sólo para el usuario actual"


;--------------------------------
; Installation details info: set registry key

LangString INFOSETREGKEY ${LANG_LATINAMERICANSPANISH} "Establecer entrada de registro de instalación: '$1' a '$0'"


;--------------------------------
;UnInstall options page, option delete accounts

LangString REMOVEPROFILES ${LANG_LATINAMERICANSPANISH} "Eliminar perfiles"


;--------------------------------
;UnInstall options page title

LangString UNINSTALLOPTIONSTITLE ${LANG_LATINAMERICANSPANISH} "Opciones de desinstalación"


;--------------------------------
;UnInstall options page header text

LangString UNINSTALLOPTIONSHEADER ${LANG_LATINAMERICANSPANISH} "Seleccione las opciones para la desinstalación de $(^Name)"


;--------------------------------
;Delete accounts question

LangString REMOVEPROFILESWARNING ${LANG_LATINAMERICANSPANISH} "¿Desea conservar los perfiles?"


;--------------------------------
;Error message when installing and application still running

LangString APPLICATIONSTILLRUNNINGINS ${LANG_LATINAMERICANSPANISH} "$(^Name) se sigue ejecutando. Cierre la aplicación antes de continuar."

;--------------------------------
; Installation details info: lync version found

LangString LYNCVERSIONFOUND ${LANG_LATINAMERICANSPANISH} "Detectada la versión de Skype Empresarial $LYNC_VERSION en el sistema."

;--------------------------------
; Installation details info: lync version not found

LangString LYNCVERSIONNOTFOUND ${LANG_LATINAMERICANSPANISH} "Skype Empresarial no disponible en el sistema."

;--------------------------------
; Installation details info: failed to write lync registry values

LangString LYNCREGISTRYNOTWRITTEN ${LANG_LATINAMERICANSPANISH} "Imposible agregar los valores de registro de Skype Empresarial como $ACCOUNT_TYPE."

;--------------------------------
;Error message when installing lync integration failed

LangString LYNCINTEGRATIONFAILED ${LANG_LATINAMERICANSPANISH} "El instalador $(^Name) no pudo preparar la integración en Skype Empresarial. Contacte con el administrador de su sistema."

;--------------------------------
; Installation details info: found valid dot net installation from system

LangString LYNCDOTNETINSTALLED ${LANG_LATINAMERICANSPANISH} "$MS_DOTNET_VERSION_TEXT encontrado."

;--------------------------------
;Error message when installing failed to find dot net installation

LangString LYNCDOTNETNOTINSTALLED ${LANG_LATINAMERICANSPANISH} "El instalador $(^Name) no pudo encontrar .NET en su sistema. La instalación no puede continuar hasta que se instale .NET Framework. Haga clic en $\"Yes$\" para descargar Framework desde Microsoft o $\"No$\" para cancelar la instalación de $(^Name)."

;OUTLOOK_PLUGIN
;--------------------------------
;UI Checkbox field text

LangString INSTALLOUTLOOKPLUGINTEXT ${LANG_LATINAMERICANSPANISH} "Instale el complemento de Outlook $(^Name). $EXTRA_CHECKBOX_INFO"
LangString INSTALLOUTLOOKPLUGINADMINTEXT ${LANG_LATINAMERICANSPANISH} "(Derechos de administrador necesarios para la instalación)"

;--------------------------------
;Error message outlook version not supported

LangString OUTLOOKPLUGINNOTSUPPORTED ${LANG_LATINAMERICANSPANISH} "No se encontró ninguna versión compatible de Microsoft Outlook. Este complemento es compatible con Microsoft Outlook 2007, 2010, 2013 y 2016 (32 y 64 bits). Por favor, instale una de estas versiones e inténtelo de nuevo."

;--------------------------------
;Warning message suggest uninstall other applications

LangString WARNINGSUGGESTUNINSTALLOTHER ${LANG_LATINAMERICANSPANISH} "Este sistema tiene aplicaciones que entran en conflicto con el complemento de Outlook $(^Name). Para una experiencia óptima, desinstale las siguientes aplicaciones: $LIST_WARNING_APPS."

;--------------------------------
;Error popup no msvs components installed

LangString MS_VSTO_REDIST_NOT_INSTALLED ${LANG_LATINAMERICANSPANISH} "Visual Studio 2010 Tools para Office Runtime no está instalado. La instalación no puede continuar hasta que se instale Runtime. Haga clic en $\"Sí$\" para descargarlo desde Microsoft o en $\"No$\" para cancelar la instalación de $(^Name)."

;---------------------------------
;Error popup Outlook still running

LangString OUTLOOKAPPRUNNING ${LANG_LATINAMERICANSPANISH} "Se detectó que el proceso OUTLOOK.EXE está utilizando algunos componentes de $(^Name). Debe cerrarlo para completar la desinstalación."

;---------------------------------
;Error popup needs reboot

LangString OUTLOOKPLUGINREBOOT ${LANG_LATINAMERICANSPANISH} "Le recomendamos encarecidamente que reinicie su equipo para finalizar la instalación."

;---------------------------------
;Error popup needs restart Lync client

LangString LYNCRUNNING ${LANG_LATINAMERICANSPANISH} "El cliente de Skype Empresarial se está ejecutando. Por favor, reinícielo para que se reflejen los cambios de instalación."
;---------------------------------
;Lync dialpad checkbox

LangString ENABLELYNCDIALPAD ${LANG_LATINAMERICANSPANISH} "Activar teclado de marcación de Skype Empresarial. $EXTRA_CHECKBOX_INFO"


;--------------------------------
;Uninstaller shortcut link title

LangString UNINSTALLERSHORTCUTTEXT ${LANG_LATINAMERICANSPANISH} "Desinstalar"

;--------------------------------
;S4B integration checkbox

LangString LYNCINTEGRATIONENABLED ${LANG_LATINAMERICANSPANISH} "Integración con Skype for Business $EXTRA_CHECKBOX_INFO"
